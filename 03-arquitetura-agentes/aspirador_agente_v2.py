import time
import random

from agentes import AgenteAbstrato


class Ambiente():
    SUJO = "X"
    LIMPO = "O"

    def __init__(self, numQuartos, inicial):
        self.quartos = [Ambiente.SUJO for i in range(numQuartos)]
        self.pos = inicial
    
    def exibir(self):
        print("Ambiente: {0} Robo em: {1}".format(self.quartos, "A" if self.pos == 0 else "B"))


#class Agente():
#    def perceber(self, ambiente):
#        raise NotImplementedError
#    
#    def definirAcao(self, percepcoes):
#        raise NotImplementedError
#    
#    def atuar(self, ambiente):
#        raise NotImplementedError

class JogadorHumano(AgenteAbstrato):
    LIMPAR = "l"
    IR_DIREITA = "d"
    IR_ESQUERDA = "e"
    
    def __init__(self):
        self.estado = 0
    
    
    def perceber(self, ambiente):
        if ambiente.quartos[ambiente.pos] == Ambiente.SUJO:
            print("Está sujo.")
        else:
            print("Está limpo.")
        
        print("Distancia do centro é: {0}".format(self.estado))
    
    def definirAcao(self, percepcao):
        acao = input("(L)impar, ir para (D)ireita ou ir para (E)squerda?").upper()
        
        if acao == "L":
            return JogadorHumano.LIMPAR
        elif acao == "D":
            self.estado += 1
            return JogadorHumano.IR_DIREITA
        else:
            self.estado -= 1
            return JogadorHumano.IR_ESQUERDA
        
    def atuar(self, acao, ambiente):
        if acao == AspiradorDePo.LIMPAR:
            ambiente.quartos[ambiente.pos] = Ambiente.LIMPO
        elif acao == AspiradorDePo.IR_DIREITA:
            ambiente.pos = min(len(ambiente.quartos)-1, ambiente.pos+1)
        elif acao == AspiradorDePo.IR_ESQUERDA:
            ambiente.pos = max(0, ambiente.pos-1)
        else:
            print("Entrada Invalida")
    
    


class AspiradorDePo(AgenteAbstrato):
    LIMPAR = "l"
    IR_DIREITA = "d"
    IR_ESQUERDA = "e"

    def __init__(self):
        pass
    
    def perceber(self, ambiente):
        if ambiente.quartos[ambiente.pos] == Ambiente.SUJO:
            print("Está sujo.")
        else:
            print("Está limpo.")
        
        print(f'Posicao: {ambiente.pos}')

        return ambiente.quartos[ambiente.pos]
    
    def definirAcao(self, percepcoes):
        if percepcoes == Ambiente.SUJO:
            return AspiradorDePo.LIMPAR
        else:
            return AspiradorDePo.IR_DIREITA
    
    def atuar(self, acao, ambiente):
        if acao == AspiradorDePo.LIMPAR:
            ambiente.quartos[ambiente.pos] = Ambiente.LIMPO
        elif acao == AspiradorDePo.IR_DIREITA:
            ambiente.pos = min(len(ambiente.quartos)-1, ambiente.pos+1)
        elif acao == AspiradorDePo.IR_ESQUERDA:
            ambiente.pos = max(0, ambiente.pos-1)
        else:
            print("Entrada Invalida")


# class AspiradorDePoComEstado(AspiradorDePo):
#     def __init__(self):
        
    
#     def definirAcao(self, percepcoes):
#         pass
    


def haSujeira(ambiente):
    for n in ambiente.quartos:
        if n == Ambiente.SUJO:
            return True
    
    return False


def iniciarSimulacao():
    ambiente = Ambiente(numQuartos = 5, inicial = 3)#random.randint(0,10))
    aspirador = AspiradorDePo()
    
    #ambiente.exibir()
    while haSujeira(ambiente):
        time.sleep(0.5)
        
        percepcoes = aspirador.perceber(ambiente)
        acao = aspirador.definirAcao(percepcoes)
        aspirador.atuar(acao, ambiente)
        
        #ambiente.exibir()


if __name__ == '__main__':
    iniciarSimulacao()