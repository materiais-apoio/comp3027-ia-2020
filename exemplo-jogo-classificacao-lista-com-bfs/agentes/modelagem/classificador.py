from agentes.modelagem.problema import ProblemaAbstrato

class ProblemaClassificador(ProblemaAbstrato):
    
    def __init__(self, estado_inicial):
        super().__init__()
        self._estado_inicial = estado_inicial

    def estado_inicial(self):
        return self._estado_inicial
    
    def acoes(self, estado):
        """
        Por simplificação, estado já é uma tupla de valores, mas não é bonito.
        """
        from acoes import AcaoJogador

        return [ AcaoJogador.permutar(i,j)
            for i,_ in enumerate(estado)
                for j,_ in enumerate(estado) ]
    
    def resultado(self, estado, acao):
        from acoes import AcoesJogador

        estado_resultante = list(estado)
        if acao.tipo == AcoesJogador.PERMUTAR:
            i, j = acao.parametros
            estado_resultante[i], estado_resultante[j] \
                = estado_resultante[j], estado_resultante[i]
        else:
            raise TypeError

        return tuple(estado_resultante)
    
    def teste_objetivo(self, estado):
        return all(estado[i] <= estado[i+1]
            for i,_ in enumerate(estado[:-1]))

    def custo_transicao(self, estado, acao, estado_resultante):
        return 1